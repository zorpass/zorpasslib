use std::time::{SystemTime, UNIX_EPOCH};

use anyhow::{Error, Result};
use scram_rs::scram::ScramParse;
use scram_rs::scram::{ScramClient, ScramNonce};
use scram_rs::scram_auth::ScramAuthClient;
use scram_rs::scram_cb::ClientChannelBindingType;
use scram_rs::scram_hashing::ScramSha256;

use super::crypto;
use crate::otp::{OTPError, OTPHandler};
use crate::registry_manager::Registry;
use crate::zorpass::account::account_client::AccountClient;
use crate::zorpass::account::{
    DeleteAccountRequest, PublicKeyRequest, RegisterRequest, SaltRequest, ScramClientFinal,
    ScramClientFirst, UpdatePasskeyRequest,
};

struct AuthClient {
    pub username: String,
    pub password: String,
}
impl ScramAuthClient for AuthClient {
    fn get_username(&self) -> &String {
        &self.username
    }
    fn get_password(&self) -> &String {
        &self.password
    }
}

pub struct AccountManager {
    username: String,
    passkey: String,

    token: Option<String>,
    credentials_expiration: u64,
    account_client: AccountClient<tonic::transport::Channel>,
    login_secrets_key: Vec<u8>,
    otp_handler: Box<dyn OTPHandler>,
}

impl AccountManager {
    async fn get_account_client() -> Result<AccountClient<tonic::transport::Channel>> {
        let channel = tonic::transport::Channel::from_static("http://localhost:41911") //from_shared(endpoint)?
            .connect()
            .await?;
        Ok(AccountClient::new(channel))
    }

    pub async fn get_salt(username: String) -> Result<Vec<u8>> {
        let mut client = Self::get_account_client().await?;
        let request = tonic::Request::new(SaltRequest { username });
        let response = client.get_salt(request).await?.into_inner();
        Ok(response.salt)
    }

    pub async fn new(
        username: String,
        passkey: String,
        login_secrets_key: Vec<u8>,
        otp_handler: Box<dyn OTPHandler>,
    ) -> Result<AccountManager> {
        log::debug!("Building new account manager for user {} with:\n\tpasskey={}\n\tlogin_secrets_key={:?}",username, passkey,login_secrets_key);
        Ok(AccountManager {
            username,
            passkey,
            token: None,
            credentials_expiration: 0,
            account_client: Self::get_account_client().await?,
            login_secrets_key,
            otp_handler,
        })
    }

    pub async fn insert_token<T>(&mut self, request: &mut tonic::Request<T>) -> Result<()> {
        let token = self.get_bearer_token().await?;
        log::debug!("Inserting token: {}", token);
        let token = tonic::metadata::MetadataValue::from_str(token.as_ref()).map_err(|e| {
            log::error!("Unable to prepare token: {}", e);
            Error::msg("Unable to prepare token")
        })?;
        request.metadata_mut().insert("authorization", token);

        Ok(())
    }

    pub async fn get_bearer_token(&mut self) -> Result<String> {
        let now = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .map_err(|e| {
                log::error!("Unable to get current timestamp: {:?}", e);
                tonic::Status::internal("internal error")
            })?
            .as_secs();

        if self.token.is_none() || self.credentials_expiration <= now {
            log::debug!(
                "Current time {} < credentials_expiration {}",
                now,
                self.credentials_expiration
            );
            self.refresh_credentials().await?;
        }

        let token = self.token.as_ref().ok_or_else(|| {
            log::error!("No token after successful refresh");
            Error::msg("Unable to refresh token")
        })?;
        Ok("Bearer: ".to_owned() + token)
    }

    pub async fn register<OPTCallback>(
        username: String,
        password: String,
        otp_callback: &mut OPTCallback,
    ) -> Result<()>
    where
        OPTCallback: FnMut(&str) -> Result<u32, OTPError>,
    {
        let keys = crypto::LoginCryptoManager::generate_keys(password.as_ref()).map_err(|e| {
            log::error!("Unable to generate keys: {:?}", e);
            Error::msg("Unable to generate keys")
        })?;

        let otp_secret = {
            // Gen OTP secret
            let secret = crypto::gen_otp_secret();
            loop {
                if let Ok(otp_verif) = otp_callback(&secret) {
                    // Check if otp_verif is valid OTP
                    let expected_otp = otp::make_totp(&secret, 30, 0)?;
                    if otp_verif == expected_otp {
                        break Ok(secret);
                    } else {
                        println!("Are you sure your computer's time is exact?");
                        log::debug!("Expected OTP was: {}", expected_otp);
                    }
                } else {
                    break Err(Error::msg("No OTP"));
                }
            }
        }?;

        let login_secrets = LoginSecrets {
            passkey: keys.passkey,
            otp_secret,
        };
        let encoded_login_secrets = rmp_serde::to_vec(&login_secrets)?;

        let login_crypto_manager = crypto::LoginCryptoManager::new(&password, &keys.user_salt)?;
        let encrypted_login_secrets =
            login_crypto_manager.encrypt_login_secrets(&encoded_login_secrets)?;
        let crypto_manager =
            crypto::CryptoManager::new(&login_crypto_manager, &keys.encrypted_master_key)?;

        // Generate and encrypt Registry
        let registry_bytes = rmp_serde::to_vec(&Registry::new())?;
        let encrypted_registry = crypto_manager
            .encrypt_with_key_id(crypto::KeyDerivation::RegistryKey, &registry_bytes)?;

        let request = tonic::Request::new(RegisterRequest {
            username,
            salt: keys.user_salt,
            encrypted_login_secrets,
            enc_public_key: keys.enc_public_key,
            encrypted_private_key: keys.encrypted_master_key,
            encrypted_registry,
        });

        let channel = tonic::transport::Channel::from_static("http://localhost:41911") //from_shared(endpoint)?
            .connect()
            .await?;
        let mut account_client = AccountClient::new(channel);
        account_client.register(request).await?;

        Ok(())
    }

    pub async fn update_passkey(&mut self, passkey: String) -> Result<()> {
        let mut request = tonic::Request::new(UpdatePasskeyRequest {
            passkey,
            login_secrets_key: self.login_secrets_key.clone(),
        });
        self.insert_token(&mut request).await?;
        self.account_client.update_passkey(request).await?;

        Ok(())
    }

    pub async fn delete_account(&mut self) -> Result<()> {
        let mut request = tonic::Request::new(DeleteAccountRequest {});
        self.insert_token(&mut request).await?;
        self.account_client.delete_account(request).await?;
        Ok(())
    }

    pub async fn get_public_key(&mut self, username: String) -> Result<Vec<u8>> {
        let mut request = tonic::Request::new(PublicKeyRequest {
            username: username.clone(),
        });
        self.insert_token(&mut request).await?;
        let result = self
            .account_client
            .get_public_key(request)
            .await?
            .into_inner();

        log::debug!("Fetched public key for user {}: {:?}", username, result);
        Ok(result.enc_public_key)
    }

    async fn refresh_credentials(&mut self) -> Result<()> {
        log::debug!("Refreshing credentials");
        let ac = AuthClient {
            username: self.username.clone(),
            password: self.passkey.clone(),
        };

        let cbt = ClientChannelBindingType::without_chan_binding();
        let nonce = ScramNonce::none();
        let scram_client = ScramClient::<ScramSha256, AuthClient>::new(&ac, nonce, cbt);
        let mut account_client = self.account_client.clone();

        match scram_client {
            Ok(mut scram_client) => {
                let client_first = scram_client.init_client(true);
                let otp = self.otp_handler.get_otp().map_err(|_| {
                    log::error!("Unable to get valid OTP");
                    Error::msg("Unable to get valid OTP")
                })?;
                log::debug!("client_first: {:?}", client_first);
                let request = tonic::Request::new(ScramClientFirst {
                    client_first,
                    login_secrets_key: self.login_secrets_key.clone(),
                    otp,
                });
                let server_response = account_client.login_init(request).await?.into_inner();
                let session_id = server_response.session_id;

                let res = scram_client.parse_response_base64(&server_response.server_first);
                match res {
                    Ok(ScramParse::Output(client_final)) => {
                        log::debug!("client_final: {:?}", client_final);
                        let request = tonic::Request::new(ScramClientFinal {
                            session_id,
                            client_final,
                        });

                        let server_response = account_client.login_end(request).await?.into_inner();
                        let res = scram_client.parse_response_base64(&server_response.server_final);
                        match res {
                            Ok(ScramParse::Completed) => match server_response.credentials {
                                Some(creds) => {
                                    log::debug!("Setting up new token: {:?}", creds);
                                    self.token = Some(creds.token);
                                    self.credentials_expiration = creds.expiration;
                                    Ok(())
                                }
                                None => {
                                    log::error!("Login response without credentials");
                                    Err(Error::msg("Internal error"))
                                }
                            },
                            Ok(ScramParse::Output(_)) => {
                                log::error!("Unexpected ScramParse::Output");
                                Err(Error::msg("Internal error"))
                            }
                            Err(e) => {
                                log::error!("Unable to parse server_final: {:?}", e);
                                Err(Error::msg("Unable to parse server_final"))
                            }
                        }
                    }
                    Err(e) => {
                        log::error!("Unable to parse server_first: {:?}", e);
                        Err(Error::msg("Unable to parse server_first"))
                    }
                    _ => {
                        log::error!("Unable to parse server_first");
                        Err(Error::msg("Unable to parse server_first"))
                    }
                }
            }
            _ => {
                log::error!("Unable to create SCRAM client");
                Err(Error::msg("Unable to create SCRAM client"))
            }
        }
    }
}

#[derive(Debug, Deserialize, Serialize)]
struct LoginSecrets {
    pub passkey: String,
    pub otp_secret: String,
}
