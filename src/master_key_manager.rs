use crate::zorpass::master_keys::master_key_client::MasterKeyClient;
use crate::zorpass::master_keys::{MasterKeyRequest, MasterKeyUpdateRequest};

use crate::account_manager::AccountManager;

#[derive(Debug)]
pub enum MasterKeyError {
    AccountError,
    GrpcError,
    TokenError,
}
impl std::error::Error for MasterKeyError {}
impl std::fmt::Display for MasterKeyError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let error = match self {
            MasterKeyError::AccountError => "account error",
            MasterKeyError::GrpcError => "GRPC error",
            MasterKeyError::TokenError => "Token Error",
        };
        write!(f, "{}", error)
    }
}

pub struct MasterKeyManager<'a> {
    account_manager: &'a mut AccountManager,
    master_key_client: MasterKeyClient<tonic::transport::Channel>,
}
impl<'a> MasterKeyManager<'a> {
    pub async fn new(
        account_manager: &'a mut AccountManager,
    ) -> Result<MasterKeyManager<'a>, MasterKeyError> {
        let channel = tonic::transport::Channel::from_static("http://localhost:41912")
            .connect()
            .await
            .map_err(|e| {
                log::error!("Unable to connect to registry server: {}", e);
                MasterKeyError::GrpcError
            })?;
        let master_key_client = MasterKeyClient::new(channel);

        Ok(MasterKeyManager {
            account_manager,
            master_key_client,
        })
    }

    pub async fn get_master_key(&mut self) -> Result<Vec<u8>, MasterKeyError> {
        log::debug!("Fetching master key");
        let mut request = tonic::Request::new(MasterKeyRequest {});
        self.account_manager
            .insert_token(&mut request)
            .await
            .map_err(|e| {
                log::error!("Unable to insert token for master key request: {}", e);
                MasterKeyError::TokenError
            })?;
        let response = self
            .master_key_client
            .get_master_key(request)
            .await
            .map_err(|e| {
                log::error!("Unable to get master key: {}", e);
                MasterKeyError::GrpcError
            })?
            .into_inner();

        Ok(response.master_key)
    }

    pub async fn update_master_key(&mut self, master_key: &[u8]) -> Result<(), MasterKeyError> {
        let mut request = tonic::Request::new(MasterKeyUpdateRequest {
            master_key: master_key.to_vec(),
        });
        self.account_manager
            .insert_token(&mut request)
            .await
            .map_err(|e| {
                log::error!("Unable to insert token for master key update: {}", e);
                MasterKeyError::TokenError
            })?;
        self.master_key_client
            .update_master_key(request)
            .await
            .map_err(|e| {
                log::error!("Unable to connect to update master key: {}", e);
                MasterKeyError::GrpcError
            })?;

        Ok(())
    }

    pub async fn update_master_key_and_passkey(
        &mut self,
        master_key: &[u8],
        passkey: String,
    ) -> Result<(), MasterKeyError> {
        self.account_manager
            .update_passkey(passkey)
            .await
            .map_err(|e| {
                log::error!("Unable to update passkey: {}", e);
                MasterKeyError::AccountError
            })?;

        self.update_master_key(&master_key).await?;

        Ok(())
    }
}
