pub mod account {
    tonic::include_proto!("account");
}

pub mod master_keys {
    tonic::include_proto!("master_keys");
}

pub mod password_registry {
    tonic::include_proto!("password_registry");
}
