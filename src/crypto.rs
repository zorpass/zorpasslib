use anyhow::Result;
use data_encoding::{BASE32_NOPAD, BASE64};
use sodiumoxide::crypto::{box_, kdf, pwhash, sealedbox, secretbox};
use sodiumoxide::randombytes::randombytes;

#[derive(Debug)]
pub enum KeyDerivation {
    PassKey,
    MasterKeyEncKey,
    LoginSecrets,
    RegistryKey,
    FileKey(u64),
}
impl KeyDerivation {
    pub fn as_kdf_id(&self) -> Result<u64, CryptoError> {
        match self {
            // Fixed ids, up to 100
            KeyDerivation::PassKey => Ok(0),
            KeyDerivation::MasterKeyEncKey => Ok(1),
            KeyDerivation::LoginSecrets => Ok(2),
            KeyDerivation::RegistryKey => Ok(3),

            // File key ids start at value 100
            // to avoid colliding fixed ids
            KeyDerivation::FileKey(id) => {
                let kdf_id = 100 + id;
                // This check prevents a possible overflow
                if kdf_id < 100 {
                    Err(CryptoError::InvalidKeyId)
                } else {
                    Ok(kdf_id)
                }
            }
        }
    }
}

#[derive(Debug)]
pub enum CryptoError {
    InvalidSalt,
    SerializationError,
    LowLevelError,
    InvalidKeyId,
}
impl std::error::Error for CryptoError {}
impl std::fmt::Display for CryptoError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let error = match self {
            CryptoError::InvalidSalt => "invalid salt",
            CryptoError::SerializationError => "Serialization Error",
            CryptoError::LowLevelError => "Low Level Error",
            CryptoError::InvalidKeyId => "Invalid Key Id",
        };
        write!(f, "{}", error)
    }
}

#[derive(Debug, PartialEq, Deserialize, Serialize)]
pub struct EncryptedData {
    pub nonce: Vec<u8>,
    pub encrypted_data: Vec<u8>,
}
impl EncryptedData {
    pub fn from_slice(data: &[u8]) -> Result<EncryptedData, CryptoError> {
        rmp_serde::from_read_ref(data).map_err(|e| {
            log::error!("unable to parse encrypted data: {}", e);
            CryptoError::SerializationError
        })
    }
    pub fn to_vec(&self) -> Result<Vec<u8>, CryptoError> {
        rmp_serde::to_vec(self).map_err(|e| {
            log::error!("unable to parse serialize encrypted data: {}", e);
            CryptoError::SerializationError
        })
    }
}

#[derive(Debug)]
pub struct GeneratedKeys {
    pub passkey: String,
    pub user_salt: Vec<u8>,
    pub encrypted_master_key: Vec<u8>,
    pub enc_public_key: Vec<u8>,
}

#[derive(Debug, PartialEq, Deserialize, Serialize)]
struct MasterKey {
    pub enc_priv_key: box_::SecretKey,
    pub derivation_key: kdf::Key,
}

pub fn gen_otp_secret() -> String {
    let secret = randombytes(16);
    BASE32_NOPAD.encode(&secret).to_ascii_uppercase()
}

pub struct LoginCryptoManager {
    derived_password: kdf::Key,
    user_salt: pwhash::Salt,
    passkey: String,
    login_secrets_key: secretbox::Key,
    master_key_enc_key: secretbox::Key,
}
impl LoginCryptoManager {
    pub fn new(password: &str, user_salt: &[u8]) -> Result<LoginCryptoManager, CryptoError> {
        let user_salt = pwhash::Salt::from_slice(&user_salt).ok_or_else(|| {
            log::error!("unable to parse salt");
            CryptoError::InvalidSalt
        })?;

        let derived_password = Self::derive_password(password, &user_salt)?;
        log::debug!(
            "Derived password from ({}, {:?}): {:?}",
            password,
            &user_salt,
            derived_password
        );
        let passkey = BASE64.encode(Self::derive_key(&derived_password, KeyDerivation::PassKey)?.as_ref());
        log::debug!("Derived passkey from {:?}", passkey);
        let login_secrets_key = Self::derive_key(&derived_password, KeyDerivation::LoginSecrets)?;
        let master_key_enc_key =
            Self::derive_key(&derived_password, KeyDerivation::MasterKeyEncKey)?;

        Ok(LoginCryptoManager {
            derived_password,
            user_salt,
            passkey,
            login_secrets_key,
            master_key_enc_key,
        })
    }

    pub fn get_passkey(&self) -> &str {
        self.passkey.as_str()
    }

    pub fn get_login_secrets_key(&self) -> Vec<u8> {
        self.login_secrets_key.as_ref().to_vec()
    }

    pub fn encrypt_login_secrets(&self, login_secrets: &[u8]) -> Result<Vec<u8>, CryptoError> {
        let nonce = secretbox::gen_nonce();
        let encrypted_data = secretbox::seal(login_secrets, &nonce, &self.login_secrets_key);
        let nonce = nonce.as_ref().to_vec();
        EncryptedData {
            nonce,
            encrypted_data,
        }
        .to_vec()
    }

    pub fn generate_keys(password: &str) -> Result<GeneratedKeys, CryptoError> {
        // Generating keys
        let enc_keys = box_::gen_keypair();
        let kdf_key = kdf::gen_key();

        let master_key = MasterKey {
            enc_priv_key: enc_keys.1,
            derivation_key: kdf_key,
        };

        // Encrypting master key
        let user_salt = pwhash::gen_salt();
        let login_crypto_manager = LoginCryptoManager::new(password, user_salt.as_ref())?;
        let encrypted_master_key = login_crypto_manager.encrypt_master_key(&master_key)?;
        let encrypted_master_key = rmp_serde::to_vec(&encrypted_master_key).map_err(|e| {
            log::error!("Unable to serialize master key: {}", e);
            CryptoError::SerializationError
        })?;
        let keys = GeneratedKeys {
            passkey: login_crypto_manager.get_passkey().to_string(),
            user_salt: user_salt.as_ref().to_vec(),
            encrypted_master_key,
            enc_public_key: enc_keys.0.as_ref().to_vec(),
        };

        log::debug!("Generated keys: {:?}", keys);

        Ok(keys)
    }

    pub fn update_master_key_password(
        &mut self,
        init_master_key: &[u8],
        new_password: &str,
    ) -> Result<Vec<u8>, CryptoError> {
        let master_key = self.decrypt_master_key(init_master_key)?;

        self.derived_password = Self::derive_password(new_password, &self.user_salt)?;
        log::debug!(
            "Derived password from ({}, {:?}): {:?}",
            new_password,
            &self.user_salt,
            self.derived_password
        );
        self.passkey = BASE64.encode(Self::derive_key(
            &self.derived_password,
            KeyDerivation::PassKey,
        )?.as_ref());
        log::debug!("Derived passkey from {:?}", self.passkey);
        self.master_key_enc_key =
            Self::derive_key(&self.derived_password, KeyDerivation::MasterKeyEncKey)?;

        let encrypted_master_key = self.encrypt_master_key(&master_key)?;
        rmp_serde::to_vec(&encrypted_master_key).map_err(|e| {
            log::error!("Unable to serialize master key: {}", e);
            CryptoError::SerializationError
        })
    }

    fn encrypt_master_key(&self, master_key: &MasterKey) -> Result<EncryptedData, CryptoError> {
        let data = rmp_serde::to_vec(master_key).map_err(|e| {
            log::error!("Unable to serialize master key: {}", e);
            CryptoError::SerializationError
        })?;

        let nonce = secretbox::gen_nonce();
        let encrypted_master_key = secretbox::seal(data.as_ref(), &nonce, &self.master_key_enc_key);
        let nonce = nonce.as_ref().to_vec();

        Ok(EncryptedData {
            nonce,
            encrypted_data: encrypted_master_key,
        })
    }

    fn decrypt_master_key(
        &self,
        raw_encrypted_master_key: &[u8],
    ) -> Result<MasterKey, CryptoError> {
        let encrypted_master_key: EncryptedData =
            rmp_serde::from_read_ref(raw_encrypted_master_key).map_err(|e| {
                log::error!("unable to parse master key: {}", e);
                CryptoError::LowLevelError
            })?;

        let ciphertext = encrypted_master_key.encrypted_data.as_ref();
        let nonce =
            secretbox::Nonce::from_slice(encrypted_master_key.nonce.as_ref()).ok_or_else(|| {
                log::error!("unable to parse nonce");
                CryptoError::LowLevelError
            })?;
        let raw_master_key = secretbox::open(ciphertext, &nonce, &self.master_key_enc_key)
            .map_err(|_| {
                log::error!("unable to decrypt master key");
                CryptoError::LowLevelError
            })?;

        rmp_serde::from_read_ref(&raw_master_key).map_err(|e| {
            log::error!("unable to parse master key: {}", e);
            CryptoError::LowLevelError
        })
    }

    fn derive_password(password: &str, user_salt: &pwhash::Salt) -> Result<kdf::Key, CryptoError> {
        let mut buffer = secretbox::Key([0; secretbox::KEYBYTES]);
        let secretbox::Key(ref mut kb) = buffer;
        let key = pwhash::derive_key(
            kb,
            password.as_bytes(),
            user_salt,
            pwhash::OPSLIMIT_INTERACTIVE,
            pwhash::MEMLIMIT_INTERACTIVE,
        )
        .map_err(|_| {
            log::error!("unable to derive from password");
            CryptoError::LowLevelError
        })?;

        kdf::Key::from_slice(key).ok_or_else(|| {
            log::error!("unable to import key derived from password as kdf key");
            CryptoError::LowLevelError
        })
    }

    fn derive_key(
        base_key: &kdf::Key,
        derivation_id: KeyDerivation,
    ) -> Result<secretbox::Key, CryptoError> {
        const CONTEXT: [u8; 8] = *b"Passkeys";

        let mut derived_key = secretbox::Key([0; secretbox::KEYBYTES]);
        kdf::derive_from_key(
            &mut derived_key.0[..],
            derivation_id.as_kdf_id()?,
            CONTEXT,
            base_key,
        )
        .map_err(|_| {
            log::error!("unable to derive password");
            CryptoError::LowLevelError
        })?;
        Ok(derived_key)
    }
}

pub struct CryptoManager {
    master_key: MasterKey,
}
impl CryptoManager {
    pub fn new(
        login_crypto_manager: &LoginCryptoManager,
        encrypted_master_key: &[u8],
    ) -> Result<CryptoManager, CryptoError> {
        let master_key = login_crypto_manager.decrypt_master_key(&encrypted_master_key)?;
        Ok(CryptoManager { master_key })
    }

    pub fn encrypt_with_key_id(
        &self,
        derivation_id: KeyDerivation,
        data: &[u8],
    ) -> Result<Vec<u8>, CryptoError> {
        let key = self.derive_key(derivation_id.as_kdf_id()?)?;
        let nonce = secretbox::gen_nonce();
        log::debug!(
            "Encrypting data with key derived from {:?}: {:?}",
            derivation_id,
            key.as_ref()
        );
        let encrypted_data = secretbox::seal(data, &nonce, &key);
        let encrypted_data = EncryptedData {
            nonce: nonce.as_ref().to_vec(),
            encrypted_data,
        }.to_vec()?;

        Ok(encrypted_data)
    }

    pub fn decrypt_with_key_id(
        &self,
        derivation_id: KeyDerivation,
        raw_encrypted_file: &[u8],
    ) -> Result<Vec<u8>, CryptoError> {
        let encrypted_file = EncryptedData::from_slice(raw_encrypted_file)?;

        let key = self.derive_key(derivation_id.as_kdf_id()?)?;
        let nonce = secretbox::Nonce::from_slice(encrypted_file.nonce.as_ref())
            .ok_or(CryptoError::LowLevelError)?;
        log::debug!(
            "Decrypting data with key derived from {:?}: {:?}",
            derivation_id,
            key.as_ref()
        );
        let decrypted_data = secretbox::open(encrypted_file.encrypted_data.as_ref(), &nonce, &key)
            .map_err(|_| {
                log::error!("Unable to decrypt data: {:?}", encrypted_file);
                CryptoError::LowLevelError
            })?;
        Ok(decrypted_data)
    }

    pub fn decrypt_private_message(
        &self,
        own_public_key: &[u8],
        data: &[u8],
    ) -> Result<Vec<u8>, CryptoError> {
        log::debug!("decrypting data with private key");

        let own_public_key = box_::PublicKey::from_slice(own_public_key).ok_or_else(|| {
            log::error!("unable to parse public key");
            CryptoError::LowLevelError
        })?;

        sealedbox::open(&data, &own_public_key, &self.master_key.enc_priv_key).map_err(|_| {
            log::error!("unable to decrypt asymmetric message");
            CryptoError::LowLevelError
        })
    }

    pub fn encrypt_with_auth(
        &self,
        recipient_public_key: &[u8],
        data: &[u8],
    ) -> Result<EncryptedData, CryptoError> {
        log::debug!("Encrypting data with recipient's public key auth");

        let recipient_public_key =
            box_::PublicKey::from_slice(recipient_public_key).ok_or_else(|| {
                log::error!("unable to parse public key");
                CryptoError::LowLevelError
            })?;

        let nonce = box_::gen_nonce();
        let encrypted_data = box_::seal(
            data,
            &nonce,
            &recipient_public_key,
            &self.master_key.enc_priv_key,
        );

        Ok(EncryptedData {
            nonce: nonce.as_ref().to_vec(),
            encrypted_data,
        })
    }

    pub fn verify_and_decrypt_private_message(
        &self,
        sender_public_key: &[u8],
        data: &[u8],
    ) -> Result<Vec<u8>, CryptoError> {
        log::debug!("Decrypting data with private key");

        let sender_public_key =
            box_::PublicKey::from_slice(sender_public_key).ok_or_else(|| {
                log::error!("unable to parse public key");
                CryptoError::LowLevelError
            })?;

        let encrypted_data = EncryptedData::from_slice(data)?;
        let nonce = box_::Nonce::from_slice(&encrypted_data.nonce).ok_or_else(|| {
            log::error!("unable to parse nonce");
            CryptoError::LowLevelError
        })?;

        box_::open(
            &encrypted_data.encrypted_data,
            &nonce,
            &sender_public_key,
            &self.master_key.enc_priv_key,
        )
        .map_err(|_| {
            log::error!("unable to decrypt asymmetric message");
            CryptoError::LowLevelError
        })
    }

    fn derive_key(&self, derivation_id: u64) -> Result<secretbox::Key, CryptoError> {
        const CONTEXT: [u8; 8] = *b"Passkeys";
        let mut derived_key = secretbox::Key([0; secretbox::KEYBYTES]);

        kdf::derive_from_key(
            &mut derived_key.0[..],
            derivation_id,
            CONTEXT,
            &self.master_key.derivation_key,
        )
        .map_err(|_| {
            log::error!("unable to derive password");
            CryptoError::LowLevelError
        })?;

        Ok(derived_key)
    }
}
