use crate::zorpass::password_registry::password_registry_client::PasswordRegistryClient;
use crate::zorpass::password_registry::{
    AddFileRequest, FileAccess, FileListRequest, FileRequest, ListeShareOffersRequest,
    RemoveFileRequest, RemoveOfferRequest, ShareFileRequest, UpdateFileListRequest,
    UpdateFileRequest,
};

use crate::account_manager::AccountManager;
use crate::crypto::{CryptoError, CryptoManager, KeyDerivation};

type VersionTag = i64;

#[derive(Debug)]
pub enum RegistryError {
    AccountError,
    CryptoError(CryptoError),
    ParsingError,
    GrpcError,
    TooManyRegistryUpdateFailure,
    PasswordAlreadyExist,
    FileNotFound,
}
impl std::error::Error for RegistryError {}
impl std::fmt::Display for RegistryError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let error = match self {
            RegistryError::AccountError => "AccountError",
            RegistryError::CryptoError(_) => "Crypto error",
            RegistryError::ParsingError => "ParsingError",
            RegistryError::GrpcError => "GrpcError",
            RegistryError::TooManyRegistryUpdateFailure => "TooManyRegistryUpdateFailure",
            RegistryError::PasswordAlreadyExist => "PasswordAlreadyExist",
            RegistryError::FileNotFound => "FileNotFound",
        };
        write!(f, "{}", error)
    }
}
impl From<CryptoError> for RegistryError {
    fn from(e: CryptoError) -> Self {
        Self::CryptoError(e)
    }
}

pub struct WebsiteAccount {
    pub website: String,
    pub username: String,
}

pub struct RegistryManager {
    account_manager: AccountManager,
    crypto_manager: CryptoManager,
    registry_client: PasswordRegistryClient<tonic::transport::Channel>,
}
impl RegistryManager {
    pub async fn new(
        account_manager: AccountManager,
        crypto_manager: CryptoManager,
    ) -> Result<RegistryManager, RegistryError> {
        let channel = tonic::transport::Channel::from_static("http://localhost:41913")
            .connect()
            .await
            .map_err(|e| {
                log::error!("Unable to connect to registry server: {}", e);
                RegistryError::GrpcError
            })?;
        let registry_client = PasswordRegistryClient::new(channel);

        Ok(RegistryManager {
            account_manager,
            crypto_manager,
            registry_client,
        })
    }

    pub async fn account_list(
        &mut self,
        pattern: Option<String>,
    ) -> Result<Vec<WebsiteAccount>, RegistryError> {
        let (file_list, _) = self.get_file_list().await?;

        let matching_entries = match pattern {
            Some(ref pattern) => file_list
                .entries
                .iter()
                .filter(|entry| entry.website.contains(pattern) || entry.username.contains(pattern))
                .collect::<Vec<&RegistryEntry>>(),
            None => file_list.entries.iter().collect(),
        };

        let files = matching_entries
            .iter()
            .map(|entry| WebsiteAccount {
                website: entry.website.to_string(),
                username: entry.username.to_string(),
            })
            .collect();

        Ok(files)
    }

    pub async fn get_account(
        &mut self,
        website: &str,
        username: &str,
    ) -> Result<Option<String>, RegistryError> {
        let (registry, _) = self.get_file_list().await?;

        match registry.get_file_details(website, username) {
            Some(file_details) => {
                let password = self
                    .get_file(
                        file_details.file_id,
                        file_details.kdf_id,
                        file_details.access_key.clone(),
                    )
                    .await?;
                Ok(Some(password))
            }
            None => Ok(None),
        }
    }

    pub async fn add_password(
        &mut self,
        website: String,
        username: String,
        file_data: &[u8],
    ) -> Result<(), RegistryError> {
        const MAX_ATTEMPTS: i32 = 3;
        for attempt in 1..=MAX_ATTEMPTS {
            log::debug!("Adding password: attempt {}/{}", attempt, MAX_ATTEMPTS);
            let (mut registry, version_tag) = self.get_file_list().await?;
            log::debug!("Fetched file_list with state: {:?}", registry);

            if registry.get_file_details(&website, &username).is_some() {
                log::error!("Entry already exists");
                return Err(RegistryError::PasswordAlreadyExist);
            }

            let kdf_id = registry.kdf_counter;
            registry.kdf_counter += 1;
            log::debug!(
                "KDF counter incremented from {} to {}",
                kdf_id,
                registry.kdf_counter
            );

            let enc_file_data = self
                .crypto_manager
                .encrypt_with_key_id(KeyDerivation::FileKey(kdf_id), file_data)?;
            log::debug!("Encrypting with key_id {}, file: {:?}", kdf_id, file_data);
            let mut request = tonic::Request::new(AddFileRequest { enc_file_data });
            self.account_manager
                .insert_token(&mut request)
                .await
                .map_err(|e| {
                    log::error!("Unable to insert token in request: {}", e);
                    RegistryError::GrpcError
                })?;
            log::debug!("Uploading file to registry");
            let file_details = self
                .registry_client
                .add_file(request)
                .await
                .map_err(|e| {
                    log::error!("Unable to add file to registry: {}", e);
                    RegistryError::GrpcError
                })?
                .into_inner();

            registry.entries.push(RegistryEntry {
                website: website.clone(),
                username: username.clone(),
                kdf_id,
                file_id: file_details.file_id,
                access_key: file_details.access_key.clone(),
            });

            log::debug!("Uploading file_list to new state: {:?}", registry);
            let file_ack = FileAccess {
                file_id: file_details.file_id,
                access_key: file_details.access_key,
            };
            let result = self
                .update_file_list(&registry, version_tag, Some(file_ack))
                .await;
            if result.is_ok() {
                return result;
            } else {
                log::warn!(
                    "Concurrent registry modification. Remaining attempts: {}",
                    MAX_ATTEMPTS - attempt
                );
            }
        }

        log::error!("Too many failed attempts");
        Err(RegistryError::TooManyRegistryUpdateFailure)
    }

    pub async fn get_file(
        &mut self,
        file_id: i32,
        key_id: u64,
        access_key: Vec<u8>,
    ) -> Result<String, RegistryError> {
        log::debug!("Fetching file id={}", file_id);
        let mut request = tonic::Request::new(FileRequest {
            file_id,
            access_key,
        });
        self.account_manager
            .insert_token(&mut request)
            .await
            .map_err(|e| {
                log::error!("Unable to insert token in request: {}", e);
                RegistryError::GrpcError
            })?;

        log::debug!("Fetching file from registry");
        let file = self
            .registry_client
            .get_file(request)
            .await
            .map_err(|e| {
                log::error!("Unable to get file from registry: {}", e);
                RegistryError::GrpcError
            })?
            .into_inner();

        log::debug!("Decrypting file id={} with key_id={}", file_id, key_id);
        let decrypted_file = self
            .crypto_manager
            .decrypt_with_key_id(KeyDerivation::FileKey(key_id), &file.enc_file_data)?;

        let password = String::from_utf8(decrypted_file).map_err(|e| {
            log::error!("Unable to add file to registry: {}", e);
            RegistryError::ParsingError
        })?;

        log::debug!("Successfully fetched file id={}", file_id);

        Ok(password)
    }

    pub async fn update_file(
        &mut self,
        website: String,
        username: String,
        file_data: &[u8],
    ) -> Result<(), RegistryError> {
        log::debug!("Fetching registry");
        let (registry, _) = self.get_file_list().await?;

        log::debug!("Fetching to find file: {}@{}", username, website);
        let file_details = registry
            .get_file_details(&website, &username)
            .ok_or_else(|| {
                log::error!("Unable to find file: {}@{}", username, website);
                RegistryError::FileNotFound
            })?;

        log::debug!("Encrypting file data");
        let file_data = self
            .crypto_manager
            .encrypt_with_key_id(KeyDerivation::FileKey(file_details.kdf_id), file_data)?;

        let mut request = tonic::Request::new(UpdateFileRequest {
            file_id: file_details.file_id,
            access_key: file_details.access_key.clone(),
            file_data,
        });
        self.account_manager
            .insert_token(&mut request)
            .await
            .map_err(|e| {
                log::error!("Unable to insert token in request: {}", e);
                RegistryError::GrpcError
            })?;

        log::debug!("Updating file");
        self.registry_client
            .update_file(request)
            .await
            .map_err(|e| {
                log::error!("Unable to update file on registry: {}", e);
                RegistryError::GrpcError
            })?;

        Ok(())
    }

    pub async fn remove_file(
        &mut self,
        website: String,
        username: String,
    ) -> Result<(), RegistryError> {
        log::debug!("Fetching registry");
        let (registry, _) = self.get_file_list().await?;

        log::debug!("Fetching to find file: {}@{}", username, website);
        let file_details = registry
            .get_file_details(&website, &username)
            .ok_or_else(|| {
                log::error!("Unable to find file: {}@{}", username, website);
                RegistryError::FileNotFound
            })?;

        let mut request = tonic::Request::new(RemoveFileRequest {
            file_id: file_details.file_id,
            access_key: file_details.access_key.clone(),
        });
        self.account_manager
            .insert_token(&mut request)
            .await
            .map_err(|e| {
                log::error!("Unable to insert token in request: {}", e);
                RegistryError::GrpcError
            })?;

        log::debug!("Updating file");
        self.registry_client
            .remove_file(request)
            .await
            .map_err(|e| {
                log::error!("Unable to remove file from registry: {}", e);
                RegistryError::GrpcError
            })?;

        log::debug!("Updating registry");
        const MAX_ATTEMPTS: i32 = 3;
        for attempt in 1..=MAX_ATTEMPTS {
            let (mut registry, version_tag) = self.get_file_list().await?;
            registry.remove_file(&website, &username);
            let result = self.update_file_list(&registry, version_tag, None).await;
            if result.is_ok() {
                return result;
            } else {
                log::warn!(
                    "Concurrent registry modification. Remaining attempts: {}",
                    MAX_ATTEMPTS - attempt
                );
            }
        }

        Ok(())
    }

    pub async fn add_share_offer(
        &mut self,
        website: String,
        username: String,
        recipient: String,
    ) -> Result<(), RegistryError> {
        log::debug!("Fetching registry");
        let (registry, _) = self.get_file_list().await?;

        log::debug!("Fetching to find file: {}@{}", username, website);
        let file_details = registry
            .get_file_details(&website, &username)
            .ok_or_else(|| {
                log::error!("Unable to find file: {}@{}", username, website);
                RegistryError::FileNotFound
            })?;

        let recipient_public_key = self
            .account_manager
            .get_public_key(recipient.clone())
            .await
            .map_err(|e| {
                log::error!("Unable to insert token in request: {}", e);
                RegistryError::GrpcError
            })?;

        let password = self
            .get_file(
                file_details.file_id,
                file_details.kdf_id,
                file_details.access_key.clone(),
            )
            .await?;

        let inner_offer = InnerShareOffer {
            website,
            username,
            password,
        };

        let serialized_inner_offer = rmp_serde::to_vec(&inner_offer).map_err(|e| {
            log::error!("unable to serialize inner offer: {}", e);
            RegistryError::ParsingError
        })?;

        let encrypted_data = self
            .crypto_manager
            .encrypt_with_auth(&recipient_public_key, &serialized_inner_offer)?
            .to_vec()?;

        // Prepare file before sharing (encrypting for recipient)
        let mut request = tonic::Request::new(ShareFileRequest {
            recipient,
            file_data: encrypted_data,
        });
        self.account_manager
            .insert_token(&mut request)
            .await
            .map_err(|e| {
                log::error!("Unable to insert token in request: {}", e);
                RegistryError::GrpcError
            })?;

        log::debug!("Sharing file file");
        self.registry_client
            .share_file(request)
            .await
            .map_err(|e| {
                log::error!("Unable to share file from registry: {}", e);
                RegistryError::GrpcError
            })?;

        Ok(())
    }

    pub async fn list_share_offers(
        &mut self,
        username: String,
    ) -> Result<Vec<ShareOffer>, RegistryError> {
        log::debug!("Fetching pending share offers");
        let mut request = tonic::Request::new(ListeShareOffersRequest {});
        self.account_manager
            .insert_token(&mut request)
            .await
            .map_err(|e| {
                log::error!("Unable to insert token in request: {}", e);
                RegistryError::GrpcError
            })?;
        let encrypted_offers = self
            .registry_client
            .list_share_offers(request)
            .await
            .map_err(|e| {
                log::error!("Unable to fetch share offer list: {}", e);
                RegistryError::GrpcError
            })?
            .into_inner()
            .share_offers;
        log::debug!("Found {} pending share offers", encrypted_offers.len());

        log::debug!("Fetching own public key");
        let own_public_key = self
            .account_manager
            .get_public_key(username)
            .await
            .map_err(|e| {
                log::error!("Unable to get own public key: {}", e);
                RegistryError::GrpcError
            })?;

        log::debug!("Decryting pending share offers (outer)");
        let outer_share_offers: Vec<(i32, OuterShareOffer)> = encrypted_offers
            .iter()
            .map(|encrypted_offer| {
                let decrypted = self
                    .crypto_manager
                    .decrypt_private_message(&own_public_key, &encrypted_offer.offer)
                    .map_err(|e| {
                        log::error!("unable to decrypt outer share offer: {}", e);
                        RegistryError::CryptoError(e)
                    })?;
                let outer_share_offer: OuterShareOffer = rmp_serde::from_read_ref(&decrypted)
                    .map_err(|e| {
                        log::error!("unable to parse encrypted data: {}", e);
                        RegistryError::ParsingError
                    })?;
                Ok((encrypted_offer.id, outer_share_offer))
            })
            .filter(|x: &Result<_, RegistryError>| x.is_ok())
            .map(|x| x.unwrap())
            .collect();

        log::debug!("Decryting pending share offers (inner)");
        let mut share_offers = vec![];
        for outer_offer in outer_share_offers.iter() {
            let sender_public_key = self
                .account_manager
                .get_public_key(outer_offer.1.sender.clone())
                .await
                .map_err(|e| {
                    log::error!("Unable to get sender's public key: {}", e);
                    RegistryError::GrpcError
                })?;
            let decrypted_inner_offer = self
                .crypto_manager
                .verify_and_decrypt_private_message(&sender_public_key, &outer_offer.1.details)?;
            let inner_share_offer: InnerShareOffer =
                rmp_serde::from_read_ref(&decrypted_inner_offer).map_err(|e| {
                    log::error!("unable to parse inner share offer: {}", e);
                    RegistryError::ParsingError
                })?;

            share_offers.push(ShareOffer {
                offer_id: outer_offer.0,
                sender: outer_offer.1.sender.clone(),
                offer: inner_share_offer,
            });
        }
        log::debug!(
            "Successfully retrieved and decrypted {} share offers",
            share_offers.len()
        );

        Ok(share_offers)
    }

    pub async fn remove_share_offer(&mut self, offer_id: i32) -> Result<(), RegistryError> {
        log::debug!("Removing share offer");
        let mut request = tonic::Request::new(RemoveOfferRequest { id: offer_id });
        self.account_manager
            .insert_token(&mut request)
            .await
            .map_err(|e| {
                log::error!("Unable to insert token in request: {}", e);
                RegistryError::GrpcError
            })?;

        log::debug!("Updating file");
        self.registry_client
            .remove_offer(request)
            .await
            .map_err(|e| {
                log::error!("Unable to remove share offer from registry: {}", e);
                RegistryError::GrpcError
            })?;

        Ok(())
    }

    async fn get_file_list(&mut self) -> Result<(Registry, VersionTag), RegistryError> {
        log::debug!("Fetching file list");

        let mut request = tonic::Request::new(FileListRequest {});
        self.account_manager
            .insert_token(&mut request)
            .await
            .map_err(|e| {
                log::error!("Unable to insert token in request: {}", e);
                RegistryError::GrpcError
            })?;

        log::debug!("Fetching file to registry");
        let response = self
            .registry_client
            .get_file_list(request)
            .await
            .map_err(|e| {
                log::error!("Unable to get file from registry: {}", e);
                RegistryError::GrpcError
            })?
            .into_inner();

        log::debug!("Decrypting file_list");
        let decrypted_file = self
            .crypto_manager
            .decrypt_with_key_id(KeyDerivation::RegistryKey, &response.file_list)?;
        let registry = rmp_serde::from_read_ref(&decrypted_file).map_err(|e| {
            log::error!("unable to parse registry: {}", e);
            CryptoError::LowLevelError
        })?;
        log::debug!("Registry with content: {:?}", registry);

        Ok((registry, response.version_tag))
    }

    async fn update_file_list(
        &mut self,
        registry: &Registry,
        version_tag: VersionTag,
        file_ack: Option<FileAccess>,
    ) -> Result<(), RegistryError> {
        log::debug!("Updating file list");
        let file_data = rmp_serde::to_vec(registry).map_err(|e| {
            log::error!("Unable to serialize registry: {}", e);
            CryptoError::SerializationError
        })?;

        log::debug!("Encrypting registry");
        let file_list = self
            .crypto_manager
            .encrypt_with_key_id(KeyDerivation::RegistryKey, &file_data)?;

        log::debug!("Uploading file to registry");
        let mut request = tonic::Request::new(UpdateFileListRequest {
            version_tag,
            file_list,
            file_ack,
        });
        self.account_manager
            .insert_token(&mut request)
            .await
            .map_err(|e| {
                log::error!("Unable to insert token in request: {}", e);
                RegistryError::GrpcError
            })?;
        self.registry_client
            .update_file_list(request)
            .await
            .map_err(|e| {
                log::error!("Unable to update registry: {}", e);
                RegistryError::GrpcError
            })?;

        Ok(())
    }
}

#[derive(Debug, Default, PartialEq, Deserialize, Serialize)]
pub struct Registry {
    kdf_counter: u64,
    entries: Vec<RegistryEntry>,
}
impl Registry {
    pub fn new() -> Registry {
        Registry {
            kdf_counter: 1, // 0 is for encrypting the registry itself
            entries: vec![],
        }
    }

    fn get_file_details(&self, website: &str, username: &str) -> Option<&RegistryEntry> {
        for entry in self.entries.iter() {
            if entry.website == website && entry.username == username {
                return Some(&entry);
            }
        }
        None
    }

    fn remove_file(&mut self, website: &str, username: &str) {
        if let Some(index) = self
            .entries
            .iter()
            .position(|entry| entry.website == website && entry.username == username)
        {
            self.entries.remove(index);
        }
    }
}

#[derive(Debug, PartialEq, Deserialize, Serialize)]
struct RegistryEntry {
    pub website: String,
    pub username: String,
    pub kdf_id: u64,
    pub file_id: i32,
    pub access_key: Vec<u8>,
}

#[derive(Debug, PartialEq, Deserialize, Serialize)]
struct OuterShareOffer {
    pub sender: String,
    pub details: Vec<u8>,
}

#[derive(Debug, PartialEq, Deserialize, Serialize)]
pub struct InnerShareOffer {
    pub website: String,
    pub username: String,
    pub password: String,
}

pub struct ShareOffer {
    pub offer_id: i32,
    pub sender: String,
    pub offer: InnerShareOffer,
}
