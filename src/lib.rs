use anyhow::Result;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate rmp_serde as rmps;

pub use account_manager::AccountManager;
pub use crypto::{CryptoManager, LoginCryptoManager};
pub use master_key_manager::MasterKeyManager;
pub use registry_manager::RegistryManager;

pub mod account_manager;
mod crypto;
pub mod master_key_manager;
pub mod registry_manager;
mod zorpass;

pub mod otp {
    pub enum OTPError {
        TooManyAttempts,
    }

    pub trait OTPHandler {
        fn get_otp(&mut self) -> Result<u32, OTPError>;
    }
}

pub async fn get_account_manager(
    username: String,
    password: String,
    otp_handler: Box<dyn otp::OTPHandler>,
) -> Result<AccountManager> {
    let salt = AccountManager::get_salt(username.clone()).await?;
    let login_crypto_manager = LoginCryptoManager::new(password.as_ref(), salt.as_ref())?;
    let passkey = login_crypto_manager.get_passkey().to_string();
    let login_secrets_key = login_crypto_manager.get_login_secrets_key();
    AccountManager::new(username, passkey, login_secrets_key, otp_handler).await
}

pub async fn get_registry_manager(
    username: String,
    password: String,
    otp_handler: Box<dyn otp::OTPHandler>,
) -> Result<RegistryManager> {
    let salt = AccountManager::get_salt(username.clone()).await?;
    let login_crypto_manager = LoginCryptoManager::new(password.as_ref(), salt.as_ref())?;
    let passkey = login_crypto_manager.get_passkey().to_string();
    let login_secrets_key = login_crypto_manager.get_login_secrets_key();
    let mut account_manager =
        AccountManager::new(username, passkey, login_secrets_key, otp_handler).await?;

    let master_key = MasterKeyManager::new(&mut account_manager)
        .await?
        .get_master_key()
        .await?;
    let crypto_manager = CryptoManager::new(&login_crypto_manager, &master_key)?;
    let registry_manager = RegistryManager::new(account_manager, crypto_manager).await?;

    Ok(registry_manager)
}
